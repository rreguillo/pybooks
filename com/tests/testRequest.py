# -*- coding: utf-8 -*-

import logging
import pdb

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

from .. import serviceRequest
from .. import serviceConstants as sc

results = []

# About book(s)
logging.info('MULTIPLE BOOKS...')
req = serviceRequest.Request(sc.USERKEY, sc.JSON)
req._setCollection(sc.BOOKS)
req._addParams({'q': 'science'})
logging.info('URL: %s\nParams: %s' % (req.url, req.params))
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE BOOK...')
req._setCollection(sc.BOOK)
req._setTarget('the_godfather')
logging.info('URL: %s' % req.url + req.target)
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

# About Author(s)
logging.info('MULTIPLE AUTHORS...')
req._setCollection(sc.AUTHORS)
req._clearParams()
req._addParams({'q': 'richards'})
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE AUTHOR...')
req._setCollection(sc.AUTHOR)
req._setTarget('puzo_mario')
logging.info('URL: %s' % req.url + req.target)
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

# About Publisher(s)
logging.info('MULTIPLE PUBLISHERS...')
req._setCollection(sc.PUBLISHERS)
req._clearParams()
req._addParams({'q': 'press'})
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE PUBLISHER...')
req._setCollection(sc.PUBLISHER)
req._setTarget('chapman_hall_crc')
logging.info('URL: %s' % req.url + req.target)
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

# About Subject(s)
logging.info('MULTIPLE SUBJECTS...')
req._setCollection(sc.SUBJECTS)
req._clearParams()
req._addParams({'q': 'disease'})
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE SUBJECT...')
req._setCollection(sc.SUBJECT)
req._setTarget('brain_diseases_diagnosis')
logging.info('URL: %s' % req.url + req.target)
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

# About category/categories
logging.info('MULTIPLE CATEGORIES...')
req._setCollection(sc.CATEGORIES)
req._clearParams()
req._addParams({'q': 'education'})
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE CATEGORY...')
req._setCollection(sc.CATEGORY)
req._setTarget('alphabetically.authors.r.i')
logging.info('URL: %s' % req.url + req.target)
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

# About book(s)
logging.info('MULTIPLE BOOKS...')
req = serviceRequest.Request(sc.USERKEY, sc.JSON, collection=sc.BOOKS, params={'q': 'science'})
logging.info('URL: %s\nParams: %s' % (req.url, req.params))
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE BOOK...')
req = serviceRequest.Request(sc.USERKEY, sc.JSON, collection=sc.BOOK, target='the_godfather')
logging.info('URL: %s\nParams: %s' % (req.url, req.params))
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('MULTIPLE BOOKS...')
req = serviceRequest.Request(sc.USERKEY, sc.JSON, collection=sc.BOOKS, params={'q': 'biol0gy'})
logging.info('URL: %s\nParams: %s' % (req.url, req.params))
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('SINGLE BOOK...')
req = serviceRequest.Request(sc.USERKEY, sc.JSON, collection=sc.BOOK, target='star_wars')
logging.info('URL: %s\nParams: %s' % (req.url, req.params))
req._send()
logging.info('[%s] %s' % (req.status_code, req.data))
results.append((req.status_code, req.rq_string))

logging.info('=== TEST RESULTS ===')
for pair in results:
    logging.info(pair)
