# -*- coding: utf-8 -*-

#TODO replace with your key
USERKEY = '6FUZ8ESC'

URL = "http://isbndb.com/api/v2/"
JSON = 'json/'
XML = 'xml/'
TYPES = [JSON, XML]

BOOK = '/book'
BOOKS = '/books'
AUTHOR = '/author'
AUTHORS = '/authors'
PUBLISHER = '/publisher'
PUBLISHERS = '/publishers'
SUBJECT = '/subject'
SUBJECTS = '/subjects'
CATEGORY = '/category'
CATEGORIES = '/categories'

APIV2_SINGLES = [
    '/book',
    '/author',
    '/publisher',
    '/subject',
    '/category'
]
APIV2_PLURAL = [
    '/books',
    '/authors',
    '/publishers',
    '/subjects',
    '/categories'
]

APIV2 = APIV2_SINGLES + APIV2_PLURAL

#JSON KEYS
DATA = 'data'
ISBN13 = 'isbn13'
ISBN10 = 'isbn10'
PUBLISHER = 'publisher_name'
PUBLISHERID = 'publisher_id'
TITLE = 'title'
TITLEL = 'title_long'
AUTHOR= 'author_data'
SUBJECT = 'subject_ids'
