# -*- coding: utf-8 -*-

import requests
import serviceConstants as sc
import json
import xml.etree.ElementTree as ET

class Request():
    url = ''
    mode = ''
    collection = ''
    params = {}
    target = ''
    status_code = None
    data = None
    rq_string = ''

    def __init__(self, userkey, rtype, collection=None, params=None, target=None):
        
        if rtype not in sc.TYPES:
            #TODO customized exception raise here
            self.mode = sc.JSON
        else:
            self.mode = rtype
        if collection:
            self.collection = collection
        if params: 
            self.params = params
        if target:
            self.target = '/' + target

        self.url = sc.URL + self.mode + userkey 

    def _setCollection(self, col):
        if col in sc.APIV2:
            self.collection = col
        else:
            #TODO customized exception raise here            
            raise KeyError
            
    def _clearParams(self):
        self.params = {}
    
    def _setTarget(self, target):
        self.target = '/' + target

    def _addParams(self, param):
        try: 
            assert type(param) == dict
            assert all(param.keys()) == True
            assert all(param.values()) == True
            assert self.collection in sc.APIV2_PLURAL
            if self.params:
                self.params.update(param)
            else:
                self.params = param
        
        except AssertionError: 
            #TODO customized exception raise here
            raise

    def _send(self):
        if self.collection in sc.APIV2_PLURAL:
            self.rq_string = self.url + self.collection
            req_result = requests.get(self.rq_string, self.params)
        else:
            self.rq_string = self.url + self.collection + self.target
            req_result = requests.get(self.rq_string, {})

        self.status_code = req_result.status_code
        if self.mode == sc.JSON:
            self.data = json.loads(req_result.text)
        elif self.mode == sc.XML:
            self.data = ET.fromstring(req_result.text)
            

            
            
