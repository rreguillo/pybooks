# -*- coding: utf-8 -*-

import logging
import pdb

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

from ..models import serviceBook as sb


book = sb(title='star_wars')
logging.info('BOOK#1: %s' %book)

book = sb(isbn13='9780451205766')
logging.info('BOOK#2: %s' %book)

book = sb(author='puzo_mario')
logging.info('BOOK#3: %s' %book)
