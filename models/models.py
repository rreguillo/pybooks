# -*- coding: utf-8 -*-

from com import serviceConstants as sc
from com import serviceRequest as sr

class serviceBook():
    isbn13=''
    isbn10=''
    title=''
    title_long = ''
    author=''
    publisher=''
    publisher_id = ''
    subject=''
    raw_data = ''

    def __init__(self, isbn13=None, title=None, author=None, publisher=None, subject=None):
        if title: 
            self.request = sr.Request(sc.USERKEY, sc.JSON, collection=sc.BOOK, target=title)
        elif isbn13:
            self.request = sr.Request(sc.USERKEY, sc.JSON, collection=sc.BOOK, target=isbn13)
        elif author:
            self.request = sr.Request(sc.USERKEY, sc.JSON, collection=sc.BOOKS,
                                      params={'q':author})        
        self.request._send()
        if self.request.status_code == 200:
            self.raw_data = self.request.data
            self._extractData()

    def _extractData(self):
        if self.request.mode == sc.XML:
            #TODO
            pass
        elif self.request.mode == sc.JSON:
            try:
                data = self.raw_data[sc.DATA]
            except KeyError:
                #TODO raise someway
                data = []
            if data: 
                values = data.pop()
            else:
                #TODO raise someway
                values = {}

            #SETUP data
            if values: 
                self.isbn13 = values[sc.ISBN13]
                self.isbn10 = values[sc.ISBN10]
                self.title = values[sc.TITLE]
                self.title_long = values[sc.TITLEL]
                self.author = values[sc.AUTHOR]
                self.publisher = values[sc.PUBLISHER]
                self.publisher_id = values[sc.PUBLISHERID]
                self.subject = values[sc.SUBJECT]
            
        else:
            raise
            #TODO Custom exception
    def __str__(self):
        result = {}
        result[sc.ISBN13] = self.isbn13
        result[sc.TITLE] = self.title
        result[sc.AUTHOR] = self.author
        result[sc.PUBLISHER] = self.publisher
        result[sc.SUBJECT] = self.subject
        return str(result)

    def __repr__(self):
        return self.__str__()

        
        
    
